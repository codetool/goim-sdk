module gitlab.com/codetool/goim-sdk

go 1.16

require (
	github.com/BurntSushi/toml v0.4.1
	github.com/Terry-Mao/goim v0.0.0-20210523140626-e742c99ad76e
	github.com/bilibili/discovery v1.2.0
	github.com/gin-gonic/gin v1.7.7
	github.com/golang/glog v1.0.0
	github.com/golang/protobuf v1.5.2
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/uuid v1.3.0
	github.com/stretchr/testify v1.7.0
	github.com/zhenjl/cityhash v0.0.0-20131128155616-cdd6a94144ab
	google.golang.org/grpc v1.43.0
	gopkg.in/Shopify/sarama.v1 v1.19.0
)
